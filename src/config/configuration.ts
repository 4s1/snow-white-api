export interface ConfigurationVariables {
  smtp?: SmtpVariables;
  timetracking?: TimetrackingVariables;
}

export interface SmtpVariables {
  username?: string;
  password?: string;
  host?: string;
}

export interface TimetrackingVariables {
  receiver?: string;
}

export default (): ConfigurationVariables => ({
  smtp: {
    host: process.env["SMTP_HOST"],
    username: process.env["SMTP_USER"],
    password: process.env["SMTP_PASSWORD"],
  },
  timetracking: {
    receiver: process.env["APP_TIMETRACKING_RECEIVER"],
  },
});
