import { Injectable, Logger } from "@nestjs/common";

interface ICacheEntry {
  date: number;
  responseData: any;
  urlBegin: string;
}

@Injectable()
export class RequestService {
  private readonly logger: Logger = new Logger(RequestService.name);

  private cache: ICacheEntry[] = [];

  public async get<T>(url: string, parameter?: string[]): Promise<T> {
    if (parameter) {
      url += `?${parameter.join("&")}`;
    }

    const dtNow: number = Date.now();
    // Remove old entries
    this.cache = this.cache.filter((entry: ICacheEntry) => entry.date > dtNow - 3000); // 3 seconds cache

    const urlBegin: string = url.substr(0, 40);
    // Find existing cache entries
    const cachedEntries: ICacheEntry[] = this.cache.filter(
      (entry: ICacheEntry) => entry.urlBegin === urlBegin,
    );
    // If any found, send cached value back
    if (cachedEntries.length > 0) {
      this.logger.warn("Response with cached value");
      return cachedEntries[0].responseData;
    }

    this.logger.log("GET request - " + url);
    const response = await fetch(url);
    const responseData = (await response.json()).data;

    this.cache.push({
      date: dtNow,
      responseData,
      urlBegin,
    });

    return responseData;
  }
}
