export interface IMetaInfo {
  timestamp: Date;
  mapVersion: string;
  moduleVersion: string;
  interfaceVersion: string;
  availableMapVersion: string[];
}

export interface ICoordinates {
  latitude: number;
  longitude: number;
}

export interface IWaypoint {
  linkId: string;
  mappedPosition: ICoordinates;
  originalPosition: ICoordinates;
  type: string;
  spot: number;
  sideOfStreet: string;
  mappedRoadName: string;
  label: string;
  shapeIndex: number;
  source: string;
}

export interface IMode {
  type: string;
  transportModes: string[];
  trafficMode: string;
  feature: any[];
}

export interface IRoadShield {
  region: string;
  category: string;
  label: string;
}

export interface IManeuver {
  position: any; // was Position
  instruction: string;
  travelTime: number;
  length: number;
  roadShield?: IRoadShield;
  roadNumber?: string;
  id: string;
  _type: string;
}

export interface ILeg {
  start: IWaypoint;
  end: IWaypoint;
  length: number;
  travelTime: number;
  maneuver: IManeuver[];
}

export interface ISummary {
  distance: number;
  trafficTime: number;
  baseTime: number;
  flags: string[];
  text: string;
  travelTime: number;
  _type: string;
}

export interface IRoute {
  waypoint: IWaypoint[];
  mode: IMode;
  leg: ILeg[];
  summary: ISummary;
  label: string[];
}

export interface IResponse {
  metaInfo: IMetaInfo;
  route: IRoute[];
  language: string;
}

export interface IHereSearchRemoteResponse {
  response: IResponse;
}
