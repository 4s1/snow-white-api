import { Module, Logger } from "@nestjs/common";
import { RemoteApiCallModule } from "./remote-api-call/remote-api-call.module";
import { SmartmirrorModule } from "./smartmirror/smartmirror.module";
import * as entities from "./entities/index";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule } from "@nestjs/config";
import configuration from "./config/configuration";

const logger = new Logger("App Module");
logger.log("Lets go");

@Module({
  controllers: [],
  imports: [
    ConfigModule.forRoot({ load: [configuration] }),
    TypeOrmModule.forRoot({
      type: "sqlite",
      database: "data/db.sqlite",
      entities: [...Object.values(entities)],
      synchronize: true,
    }),
    RemoteApiCallModule,
    SmartmirrorModule,
  ],
  providers: [],
})
export class AppModule {}
