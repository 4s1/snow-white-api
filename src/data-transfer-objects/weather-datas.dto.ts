import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { WeatherDataDto } from "./weather-data.dto";

export class WeatherDatasDto {
  @IsNotEmpty()
  @ApiProperty()
  public name!: string;

  @IsNotEmpty()
  @ApiProperty()
  public infos!: WeatherDataDto[];
}
