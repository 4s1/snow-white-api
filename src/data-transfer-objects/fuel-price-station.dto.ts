import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class FuelPriceStationDto {
  @IsNotEmpty()
  @ApiProperty()
  public id!: string;

  @IsNotEmpty()
  @ApiProperty()
  public remoteId!: string;

  @IsNotEmpty()
  @ApiProperty()
  public name!: string;

  @IsNotEmpty()
  @ApiProperty()
  public nameOrigin!: string;

  @IsNotEmpty()
  @ApiProperty()
  public latitude!: number;

  @IsNotEmpty()
  @ApiProperty()
  public longitude!: number;

  @ApiProperty()
  public sortNo!: number;
}
