import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { RmvTripDto } from "./rmv-trip.dto";

// ToDo: Bessere Namen für RmvTripsDto und RmvTripDto wählen.
export class RmvTripsDto {
  @IsNotEmpty()
  @ApiProperty()
  public text!: string;

  @IsNotEmpty()
  @ApiProperty()
  public trips!: RmvTripDto[];
}
