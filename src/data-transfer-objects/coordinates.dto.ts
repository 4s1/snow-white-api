import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CoordinatesDto {
  @IsNotEmpty()
  @ApiProperty({ example: 50.1069748 })
  public latitude!: number;

  @IsNotEmpty()
  @ApiProperty({ example: 8.6651277 })
  public longitude!: number;
}
