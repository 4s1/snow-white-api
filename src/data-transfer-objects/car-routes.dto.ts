import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { CarRouteDto } from "./car-route.dto";

// ToDo: Bessere Namen für CarRoutesDto und CarRouteDto wählen.
export class CarRoutesDto {
  @IsNotEmpty()
  @ApiProperty()
  public text!: string;

  @IsNotEmpty()
  @ApiProperty()
  public routes!: CarRouteDto[];
}
