import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class WeatherSettingsDto {
  @IsNotEmpty()
  @ApiProperty({ example: true })
  public isActive!: boolean;

  @ApiProperty({ example: "Lorem Ipsum" })
  public apiKey!: string;

  @ApiProperty()
  public locationId!: string | null;
}
