import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class OpenStreetMapLocationDto {
  @IsNotEmpty()
  @ApiProperty()
  public remoteId!: number;

  @IsNotEmpty()
  @ApiProperty()
  public name!: string;

  @IsNotEmpty()
  @ApiProperty()
  public latitude!: number;

  @IsNotEmpty()
  @ApiProperty()
  public longitude!: number;

  @ApiProperty()
  public importance!: number;
}
