import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class TankerkoenigStationDto {
  @IsNotEmpty()
  @ApiProperty()
  public remoteId!: string;

  @IsNotEmpty()
  @ApiProperty()
  public name!: string;

  @ApiProperty()
  public brand!: string;

  @ApiProperty()
  public street!: string;

  @ApiProperty()
  public city!: string;

  @IsNotEmpty()
  @ApiProperty()
  public latitude!: number;

  @IsNotEmpty()
  @ApiProperty()
  public longitude!: number;

  @ApiProperty()
  public houseNumber!: string;

  @ApiProperty()
  public postCode!: number;
}
