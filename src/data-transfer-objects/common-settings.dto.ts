import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CommonSettingsDto {
  @IsNotEmpty()
  @ApiProperty()
  public morningStart!: number;

  @IsNotEmpty()
  @ApiProperty()
  public morningEnd!: number;
}
