import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
export class SortOrderDto {
  @IsNotEmpty()
  @ApiProperty()
  public id!: string;

  @IsNotEmpty()
  @ApiProperty()
  public sortNo!: number;
}
