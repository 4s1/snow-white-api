import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class DateSettingsDto {
  @IsNotEmpty()
  @ApiProperty()
  public isActive!: boolean;

  @IsNotEmpty()
  @ApiProperty()
  public pattern!: string;

  @IsNotEmpty()
  @ApiProperty({ example: 12 })
  public fontSize!: number;
}
