import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CarRouteDto {
  @IsNotEmpty()
  @ApiProperty()
  public distance!: number;

  @IsNotEmpty()
  @ApiProperty()
  public expectedTime!: number;

  @IsNotEmpty()
  @ApiProperty()
  public text!: string;

  @IsNotEmpty()
  @ApiProperty()
  public streetTypes!: string[];
}
