import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UiSettingsDto {
  @IsNotEmpty()
  @ApiProperty()
  public traffic!: {
    isActive: boolean;
  };

  @IsNotEmpty()
  @ApiProperty()
  public date!: {
    isActive: boolean;
    fontSize: number;
  };

  @IsNotEmpty()
  @ApiProperty()
  public weather!: {
    isActive: boolean;
  };

  @IsNotEmpty()
  @ApiProperty()
  public timetable!: {
    isActive: boolean;
  };

  @IsNotEmpty()
  @ApiProperty()
  public fuelPrice!: {
    isActive: boolean;
    showE5: boolean;
    showE10: boolean;
    showDiesel: boolean;
    interval: number;
  };
}
