import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class TrafficSettingsDto {
  @IsNotEmpty()
  @ApiProperty({ example: true })
  public isActive!: boolean;

  @ApiProperty({ example: "Lorem Ipsum" })
  public apiKey!: string;

  @ApiProperty()
  public locationFromId!: string | null;

  @ApiProperty()
  public locationToId!: string | null;
}
