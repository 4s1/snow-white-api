import { Controller, Get, Put, Body } from "@nestjs/common";
import { DateSettingsService } from "./settings/date-settings.service";
import { DateSettingsDto } from "../../../data-transfer-objects/date-settings.dto";
import { ApiOperation } from "@nestjs/swagger";

@Controller("/v1/smartmirror/admin/date")
export class DateController {
  constructor(private readonly settings: DateSettingsService) {}

  // GET - /v1/smartmirror/admin/date/settings
  @Get("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | date"],
  })
  public async loadSettings(): Promise<DateSettingsDto> {
    return this.settings.load();
  }

  // PUT - /v1/smartmirror/admin/date/settings
  @Put("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | date"],
  })
  public async saveSettings(@Body() body: DateSettingsDto): Promise<void> {
    return this.settings.save(body);
  }
}
