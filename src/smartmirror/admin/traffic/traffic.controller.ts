import { Controller, Get, Put, Logger, Body } from "@nestjs/common";
import { TrafficSettingsService } from "./settings/traffic-settings.service";
import { TrafficSettingsDto } from "../../../data-transfer-objects/traffic-settings.dto";
import { ApiOperation } from "@nestjs/swagger";

// ToDo: Namen korrigieren
@Controller("/v1/smartmirror/admin/traffic")
export class TrafficController {
  private readonly logger: Logger = new Logger(TrafficController.name);

  constructor(private readonly settings: TrafficSettingsService) {}

  // GET - /v1/smartmirror/admin/traffic/settings
  @Get("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | traffic"],
  })
  public loadSettings(): Promise<TrafficSettingsDto> {
    return this.settings.load();
  }

  // PUT - //v1/smartmirror/admin/traffic/settings
  @Put("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | traffic"],
  })
  public saveSettings(@Body() settings: TrafficSettingsDto): Promise<void> {
    return this.settings.save(settings);
  }
}
