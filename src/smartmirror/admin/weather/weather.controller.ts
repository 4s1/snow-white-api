import { Controller, Get, Put, Logger, Body } from "@nestjs/common";
import { WeatherSettingsService } from "./settings/weather-settings.service";
import { WeatherSettingsDto } from "../../../data-transfer-objects/weather-settings.dto";
import { ApiOperation } from "@nestjs/swagger";

@Controller("/v1/smartmirror/admin/weather")
export class WeatherController {
  private readonly logger: Logger = new Logger(WeatherController.name);

  constructor(private readonly settings: WeatherSettingsService) {}

  // GET - /v1/smartmirror/admin/weather/settings
  @Get("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | weather"],
  })
  public loadSettings(): Promise<WeatherSettingsDto> {
    return this.settings.load();
  }

  // PUT - //v1/smartmirror/admin/weather/settings
  @Put("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | weather"],
  })
  public saveSettings(@Body() settings: WeatherSettingsDto): Promise<void> {
    return this.settings.save(settings);
  }
}
