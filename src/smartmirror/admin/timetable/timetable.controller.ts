import { Controller, Get, Put, Post, Delete, Logger, Body, Param } from "@nestjs/common";
import { TimetableSettingsService } from "./settings/timetable-settings.service";
import { TimetableStationsService } from "./stations/timetable-stations.service";
import { RmvStationDto } from "../../../data-transfer-objects/rmv-station.dto";
import { CoordinatesDto } from "../../../data-transfer-objects/coordinates.dto";
import { TimetableStationDto } from "../../../data-transfer-objects/timetable-station.dto";
import { SortOrderDto } from "../../../data-transfer-objects/sort-order.dto";
import { TimetableSettingsDto } from "../../../data-transfer-objects/timetable-settings.dto";
import { ApiOperation } from "@nestjs/swagger";

@Controller("/v1/smartmirror/admin/timetable")
export class TimetableController {
  private readonly logger: Logger = new Logger(TimetableController.name);

  constructor(
    private readonly settings: TimetableSettingsService,
    private readonly stations: TimetableStationsService,
  ) {}

  // GET - /v1/smartmirror/admin/timetable/settings
  @Get("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public loadSettings(): Promise<TimetableSettingsDto> {
    return this.settings.load();
  }

  // PUT - /v1/smartmirror/admin/timetable/settings
  @Put("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public saveSettings(@Body() settings: TimetableSettingsDto): Promise<void> {
    return this.settings.save(settings);
  }

  // PUT - /v1/smartmirror/admin/timetable/stations/reorder
  @Put("/stations/reorder")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public reorderStations(@Body() body: SortOrderDto[]): Promise<void> {
    return this.stations.reorderStations(body);
  }

  // GET - /v1/smartmirror/admin/timetable/stations/search
  @Post("/stations/search")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public async searchForStations(@Body() coordinates: CoordinatesDto): Promise<RmvStationDto[]> {
    const apiKey = (await this.settings.getRecord()).apiKey || process.env.APIKEY_RMV || "";
    return this.stations.search(apiKey, coordinates.latitude, coordinates.longitude);
  }

  // POST - /v1/smartmirror/admin/timetable/stations
  @Post("/stations")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public createStation(@Body() station: RmvStationDto): Promise<void> {
    return this.stations.add(station);
  }

  // GET - /v1/smartmirror/admin/timetable/stations
  @Get("/stations")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public loadAllStations(): Promise<TimetableStationDto[]> {
    return this.stations.loadAll();
  }

  // GET - /v1/smartmirror/admin/timetable/stations/:id
  @Get("/stations/:id")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public loadSingleStation(@Param("id") id: string): Promise<TimetableStationDto | null> {
    return this.stations.loadSingle(id);
  }

  // GET - /v1/smartmirror/admin/timetable/stations/:id
  @Put("/stations/:id")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public saveStation(@Param("id") id: string, @Body() station: TimetableStationDto): Promise<void> {
    return this.stations.save(id, station);
  }

  // DELETE - /v1/smartmirror/admin/timetable/stations/:id
  @Delete("/stations/:id")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | timetable"],
  })
  public deleteStation(@Param("id") id: string): Promise<void> {
    return this.stations.delete(id);
  }
}
