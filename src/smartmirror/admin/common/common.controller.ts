import { Controller, Get, Post, Delete, Put, Param, Body } from "@nestjs/common";
import { CommonLocationsService } from "./locations/common-locations.service";
import { OpenStreetMapLocationDto } from "../../../data-transfer-objects/open-street-map-location.dto";
import { CommonSettingsService } from "./settings/common-settings.service";
import { CommonSettingsDto } from "../../../data-transfer-objects/common-settings.dto";
import { CommonLocationDto } from "../../../data-transfer-objects/common-location.dto";
import { SortOrderDto } from "../../../data-transfer-objects/sort-order.dto";
import { ApiOperation } from "@nestjs/swagger";

@Controller("/v1/smartmirror/admin/common")
export class CommonController {
  constructor(
    private readonly location: CommonLocationsService,
    private readonly settings: CommonSettingsService,
  ) {}

  // GET - /v1/smartmirror/admin/common/settings
  @Get("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public loadSettings(): Promise<CommonSettingsDto> {
    return this.settings.load();
  }

  // PUT - /v1/smartmirror/admin/common/settings
  @Put("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public saveSettings(@Body() body: CommonSettingsDto): Promise<void> {
    return this.settings.save(body);
  }

  // GET - /v1/smartmirror/admin/common/locations/search/:text
  @Get("/locations/search/:text")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public searchLocations(@Param("text") text: string): Promise<OpenStreetMapLocationDto[]> {
    return this.location.search(text);
  }

  // POST - /v1/smartmirror/admin/common/locations
  @Post("/locations")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public addLocation(@Body() body: OpenStreetMapLocationDto): Promise<void> {
    return this.location.add(body);
  }

  // GET - /v1/smartmirror/admin/common/locations
  @Get("/locations")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public loadAllLocations(): Promise<CommonLocationDto[]> {
    return this.location.loadAll();
  }

  // PUT - /v1/smartmirror/admin/common/locations/reorder
  @Put("/locations/reorder")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public reorderLocations(@Body() body: SortOrderDto[]): Promise<void> {
    return this.location.reorderLocations(body);
  }

  // GET - /v1/smartmirror/admin/common/locations/:id
  @Get("/locations/:id")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public loadSingleLocation(@Param("id") id: string): Promise<CommonLocationDto | null> {
    return this.location.loadSingle(id);
  }

  // PUT - /v1/smartmirror/admin/common/locations/:id
  @Put("/locations/:id")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public saveLocation(@Param("id") id: string, @Body() body: CommonLocationDto): Promise<void> {
    return this.location.save(id, body);
  }

  // DELETE - /v1/smartmirror/admin/common/locations/:id
  @Delete("/locations/:id")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | admin | common"],
  })
  public deleteLocation(@Param("id") id: string): Promise<void> {
    return this.location.delete(id);
  }
}
