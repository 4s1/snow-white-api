import { Controller, Get, Logger } from "@nestjs/common";
import { UiFuelPriceService } from "./fuel-price/ui-fuel-price.service";
import { UiDateService } from "./date/ui-date.service";
import { UiTimetableService } from "./timetable/ui-timetable.service";
import { UiTrafficService } from "./traffic/ui-traffic.service";
import { UiWeatherService } from "./weather/ui-weather.service";
import { CarRoutesDto } from "../../data-transfer-objects/car-routes.dto";
import { RmvTripsDto } from "../../data-transfer-objects/rmv-trips.dto";
import { FuelPricePricesDto } from "../../data-transfer-objects/fuel-price-prices.dto";
import { UiSettingsService } from "./settings/ui-settings.service";
import { UiSettingsDto } from "../../data-transfer-objects/ui-settings.dto";
import { WeatherDatasDto } from "../../data-transfer-objects/weather-datas.dto";
import { ApiOperation } from "@nestjs/swagger";

@Controller("/v1/smartmirror/ui")
export class UiController {
  private readonly logger: Logger = new Logger(UiController.name);

  constructor(
    private readonly settings: UiSettingsService,
    private readonly date: UiDateService,
    private readonly fuelPrice: UiFuelPriceService,
    private readonly timetable: UiTimetableService,
    private readonly traffic: UiTrafficService,
    private readonly weather: UiWeatherService,
  ) {}

  // GET - /v1/smartmirror/ui/fuelprice
  @Get("/fuelprice")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | ui"],
  })
  public getFuelPrices(): Promise<FuelPricePricesDto[]> {
    return this.fuelPrice.getPrices();
  }

  // GET - /v1/smartmirror/ui/date
  @Get("/date")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | ui"],
  })
  public loadDate(): Promise<string> {
    return this.date.getPattern();
  }

  // GET - /v1/smartmirror/ui/timetable
  @Get("/timetable")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | ui"],
  })
  public loadTimetable(): Promise<RmvTripsDto> {
    return this.timetable.getTimetable();
  }

  // GET - /v1/smartmirror/ui/weather
  @Get("/weather")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | ui"],
  })
  public loadWeather(): Promise<WeatherDatasDto> {
    return this.weather.getWeather();
  }

  // GET - /v1/smartmirror/ui/traffic
  @Get("/traffic")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | ui"],
  })
  public loadTraffic(): Promise<CarRoutesDto> {
    return this.traffic.getRoute();
  }

  // GET - /v1/smartmirror/ui/settings
  @Get("/settings")
  @ApiOperation({
    description: "",
    summary: "",
    tags: ["smartmirror | ui"],
  })
  public loadSettings(): Promise<UiSettingsDto> {
    return this.settings.load();
  }
}
