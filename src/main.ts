import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { SwaggerModule, DocumentBuilder, OpenAPIObject } from "@nestjs/swagger";
import { ValidationPipe } from "@nestjs/common";
import { AllExceptionsFilter } from "./AllExceptionsFilter";
import { FastifyAdapter, NestFastifyApplication } from "@nestjs/platform-fastify";

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());

  app.useGlobalFilters(new AllExceptionsFilter());
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors({
    methods: ["GET", "PUT", "POST", "PATCH", "DELETE"],
  });

  initSwagger(app);
  await app.listen(3000, "0.0.0.0");
}
bootstrap();

function initSwagger(app: NestFastifyApplication): void {
  const options = new DocumentBuilder()
    .setTitle("API")
    .setDescription("API Documentation")
    .setVersion("1.1.0")
    .addTag("smartmirror | admin | weather", "")
    .addTag("smartmirror | admin | timetable", "")
    .addTag("smartmirror | admin | fuelprice", "")
    .addTag("smartmirror | admin | common", "")
    .addTag("smartmirror | admin | date", "")
    .addTag("smartmirror | admin | traffic", "")
    .addTag("smartmirror | ui", "")
    .build();
  const document: OpenAPIObject = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("apidoc", app, document);
}
