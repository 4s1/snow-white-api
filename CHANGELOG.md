# Changelog

## 1.2.5 (2022-04-03)

## 1.2.4 (2021-12-03)

## 1.2.3 (2021-12-03)

- added missing log message

## 1.2.2 (2021-11-21)

## 1.2.1 (2021-11-13)

### Bug Fixes

- fix(mqtt): connect only if server is set
- fix(timetable): apikey was not set correct
- fix(traffic): apikey was not set correct
- fix(weather): apikey was not set correct

## 1.2.0 (2021-11-07)

## 1.1.1 (2021-11-01)

## 1.1.0 (2021-11-01)

- feat: implement mqtt client
- feat: sqlite database can be used

- fix: url in package.json
