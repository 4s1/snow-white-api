# Snow White - API

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

_Currently the project does not work. I am working on it._

## ToDo

- [x] Enable Renovate Bot
- [x] Create DTO folder
- [ ] Integrate Rapunzel
- [x] Enable pipeline
- [ ] Generate OpenAPI Items example for frontend
- [x] Prettier + 4s1 config seems broken
- [ ] 4s1 tsconfig
- [ ] ESM
- [x] use fastify
- [x] Write good API (with tags)
- [ ] tsconfig for tests
- [x] Remove moment
- [ ] Docker publish
- [x] downgrade @types/node
- [x] Update ConfigService
- [ ] Remove local ESLint rules
