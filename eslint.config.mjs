import customConfig from "@4s1/eslint-config/eslint.config.mjs";

const config = [
  ...customConfig,
  {
    rules: {
      "@typescript-eslint/consistent-type-imports": "warn",
    },
  },
];

export default config;
